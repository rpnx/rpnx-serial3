# rpnx-serial3

A CMake library for doing serialization.


Copyright © 2022, Ryan P. Nicholl, <rnicholl@protonmail.com>
All Rights Reserved, License: RPNX Permissive License, Version 1
