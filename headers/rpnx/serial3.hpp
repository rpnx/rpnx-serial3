//
// Created by rnicholl on 1/17/22.
//

// TODO: Need to add a lot of overflow checks when object size and format size are not the same
// Also check LE to make sure it works correctly.

#ifndef RPNX_SERIAL_SERIAL_INTERFACE_HPP
#define RPNX_SERIAL_SERIAL_INTERFACE_HPP

#include <cinttypes>
#include <concepts>
#include <deque>
#include <map>
#include <set>
#include <tuple>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>

namespace rpnx
{
    namespace serial3
    {
        class io_proxy final
        {
            std::byte value;

          public:
            constexpr io_proxy(std::byte v) noexcept
                : value(v)
            {
            }

            template < std::integral I >
            constexpr io_proxy(I v) noexcept
                : value(std::byte(v))
            {
                // static_assert(sizeof(I) == 1);
            }

            inline constexpr operator std::byte() const noexcept
            {
                return value;
            }

            inline constexpr operator std::uint8_t() const noexcept
            {
                return std::uint8_t(value);
            }
        };

        template < typename T >
        concept vectorlike = requires(T a) {
                                 {
                                     a.size()
                                     } -> std::convertible_to< std::size_t >;
                                 a.push_back(typename T::value_type());
                                 {
                                     typename T::value_type()
                                 };
                             };

        template < typename T >
        concept arraylike = requires(T a) {
                                {
                                    a.size()
                                    } -> std::convertible_to< std::size_t >;
                                {
                                    typename T::value_type()
                                };
                                {
                                    std::tuple_size< T >::value == a.size()
                                };
                                // static_assert(std::tuple_size<S>::value == a.size());
                            };

        template < typename T >
        concept setlike = requires(T a) {
                              {
                                  a.size()
                                  } -> std::convertible_to< std::size_t >;
                              a.insert(typename T::value_type());
                              [](T const& a)
                              {
                for (typename T::value_type const t : a)
                {
                }
                                  }(a);
                              a.clear();
                              a.count(typename T::value_type());
                          };

        template < typename T >
        concept maplike = requires(T a) {
                              a[typename T::key_type()];
                              {
                                  a.size()
                                  } -> std::convertible_to< std::size_t >;
                              a.insert(typename T::value_type());
                              [](T const& a)
                              {
                for (typename T::value_type const t : a)
                {
                }
                                  }(a);
                              a.clear();
                          };

        static_assert(maplike< std::map< int, int > >);
        static_assert(!maplike< std::set< int > >);
        static_assert(setlike< std::set< int > >);
        static_assert(!setlike< std::map< int, int > >);

        template < std::size_t N >
        struct format_little_endian
        {
        };

        template < std::size_t N >
        struct format_big_endian
        {
        };

        template < typename... Ts >
        struct format_sequence
        {
        };

        template < typename T, std::size_t N >
        struct format_array
        {
        };

        struct format_intany
        {
        };

        struct format_uintany
        {
        };

        struct format_intvar
        {
        };

        struct format_uintvar
        {
        };

        template < typename TFmt, typename T, T Value >
        struct format_sentinel_terminated_list
        {
        };

        template < typename... Ts >
        struct format_json_object
        {
        };

        template < typename T >
        struct format_json_field;

        template < typename TCount = format_uintany >
        struct format_utf8
        {
        };

        template < typename TCount = format_uintany >
        struct format_native_mbcs
        {
        };

        template < typename TElements, typename TCount = format_uintany >
        struct format_count_list
        {
        };

        template < typename T >
        struct default_format;

        template < typename T >
        struct serialize_binding;

        template < typename T >
        struct deserialize_binding;

        template < typename T >
        concept tieable = requires(T a) { a.tie(); };

        template < tieable T >
        struct serialize_binding< T >
        {
          private:
            T const* m_ptr;

          public:
            constexpr serialize_binding() noexcept
                : m_ptr(nullptr)
            {
            }

            void initialize(T const& t)
            {
                m_ptr = &t;
            }

            auto use() const
            {
                return m_ptr->tie();
            }

            void finalize() const noexcept
            {
            }
        };

        template < tieable T >
        class deserialize_binding< T >
        {
            T* m_ptr;

          public:
            constexpr deserialize_binding() noexcept
                : m_ptr(nullptr)
            {
            }

            void initialize(T& t)
            {
                m_ptr = &t;
            }

            auto use()
            {
                return m_ptr->tie();
            }

            void finalize() const noexcept
            {
            }
        };

        template < maplike M >
        struct default_format< M >;

        template <>
        struct default_format< char8_t >;

        template <>
        struct default_format< std::byte >;

        template < std::unsigned_integral I >
        struct default_format< I >;

        template <>
        struct default_format< char8_t >
        {
            using type = format_little_endian< 8 >;
        };

        template <>
        struct default_format< std::byte >
        {
            using type = std::byte;
        };

        template < std::unsigned_integral I >
        struct default_format< I >
        {
            using type = format_little_endian< sizeof(I) * 8 >;
        };

        template < std::signed_integral I >
        struct default_format< I >
        {
            using type = format_little_endian< sizeof(I) * 8 >;
        };

        template < typename T >
        struct default_format< T const >
        {
            using type = typename default_format< T >::type;
        };

        template < typename T >
        struct default_format< T&& >
        {
            using type = typename default_format< T >::type;
        };

        template < typename T >
        struct default_format< T& >
        {
            using type = typename default_format< T >::type;
        };

        template < typename T >
        struct default_format< T const& >
        {
            using type = typename default_format< T >::type;
        };

        template < setlike S >
        struct default_format< S >
        {
            static_assert(!maplike< S >);
            using type = format_count_list< typename default_format< typename S::value_type >::type >;
        };

        template < vectorlike V >
        struct default_format< V >
        {
            using type = format_count_list< typename default_format< typename V::value_type >::type >;
        };

        template < arraylike A >
        struct default_format< A >
        {
            using type = format_array< typename default_format< typename A::value_type >::type, std::tuple_size< A >::value >;
        };

        template <>
        struct default_format< std::u8string >
        {
            using type = format_utf8<>;
        };

        static_assert(std::is_same_v< typename default_format< std::u8string >::type, format_utf8<> >);

        template < typename T, typename A >
        struct default_format< std::deque< T, A > >
        {
            using type = format_count_list< T >;
        };

        template < typename... Ts >
        struct default_format< std::tuple< Ts... > >
        {
            using type = format_sequence< typename default_format< Ts >::type... >;
        };

        template < maplike M >
        struct default_format< M >
        {

            static_assert(!setlike< M >);
            using type = format_count_list< typename default_format< typename M::value_type >::type >;
        };

        template < typename A, typename B >
        struct default_format< std::pair< A, B > >
        {
            using type = format_sequence< typename default_format< A >::type, typename default_format< B >::type >;
        };

        static_assert(std::is_same_v< typename default_format< std::tuple< int& > >::type, format_sequence< format_little_endian< 32 > > >);

        template < typename Format >
        struct format_has_fixed_serial_size;

        template < size_t N >
        struct format_has_fixed_serial_size< format_little_endian< N > >
        {
            using type = std::true_type;
        };

        template < size_t N >
        struct format_has_fixed_serial_size< format_big_endian< N > >
        {
            using type = std::true_type;
        };

        template <>
        struct format_has_fixed_serial_size< format_uintany >
        {
            using type = std::false_type;
        };

        template <>
        struct format_has_fixed_serial_size< format_intany >
        {
            using type = std::false_type;
        };

        template <>
        struct format_has_fixed_serial_size< format_uintvar >
        {
            using type = std::false_type;
        };

        template <>
        struct format_has_fixed_serial_size< format_intvar >
        {
            using type = std::false_type;
        };

        template < typename T >
        concept bindable = requires(T a) {
                               {
                                   serialize_binding< T >().initialize(a)
                               };
                               {
                                   serialize_binding< T >().use()
                               };
                               {
                                   serialize_binding< T >().finalize()
                               };
                               {
                                   deserialize_binding< T >().initialize(a)
                               };
                               {
                                   deserialize_binding< T >().use()
                               };
                               {
                                   deserialize_binding< T >().finalize()
                               };
                           };

        template < typename T >
        concept not_bindable = !
        bindable< T >;

        template < typename T >
        concept with_default_format_type_member = requires { typename T::default_format; };

        template < with_default_format_type_member T >
        struct default_format< T >
        {
            using type = typename T::default_format;
        };

        template < typename T >
        concept bindable_without_default_format_type_member = bindable< T > && !
        with_default_format_type_member< T >;

        template < typename T, typename Format = typename default_format< T >::type >
        class serializer;

        template < typename... Ts, typename... Ts2 >
        class serializer< std::tuple< Ts... >, format_sequence< Ts2... > >;

        template < typename Fsz >
        class serializer< std::u8string, format_utf8< Fsz > >;

        template < maplike M, typename FK, typename FV, typename Fsz >
        class serializer< M, format_count_list< format_sequence< FK, FV >, Fsz > >;

        template < setlike S, typename Ft, typename Fsz >
        class serializer< S, format_count_list< Ft, Fsz > >;

        template < bindable B, typename Fmt >
        class serializer< B, Fmt >;

        template < std::integral I, std::size_t N >
        class serializer< I, format_little_endian< N > >;

        // template <typename S, typename F, typename It>
        // class generator_serializer;

        template < bindable_without_default_format_type_member B >
        struct default_format< B >
        {
            using type = typename default_format< std::remove_cvref_t< decltype(serialize_binding< B >().use()) > >::type;
        };

        template < bindable B, typename Fmt >
        class serializer< B, Fmt >
        {
            using serialize_binding_type = std::remove_cvref_t< decltype(serialize_binding< B >().use()) >;

            using deserialize_binding_type = std::remove_cvref_t< decltype(deserialize_binding< B >().use()) >;

          public:
            template < typename It >
            static auto serialize_iter(B const& input, It it) -> It
            {
                serialize_binding< B > bind;
                bind.initialize(input);
                it = serializer< serialize_binding_type, Fmt >::serialize_iter(bind.use(), it);
                bind.finalize();
                return it;
            }

            template < typename It >
            static auto serialize_iter(B const& input, It it, It it_end) -> It
            {
                serialize_binding< B > bind;
                bind.initialize(input);
                it = serializer< serialize_binding_type, Fmt >::serialize_iter(bind.use(), it, it_end);
                bind.finalize();
                return it;
            }

            template < typename It >
            static auto deserialize_iter(B& output, It it) -> It
            {
                deserialize_binding< B > bind;
                bind.initialize(output);
                it = serializer< deserialize_binding_type, Fmt >::deserialize_iter(bind.use(), it);
                bind.finalize();
                return it;
            }

            template < typename It >
            static auto deserialize_iter(B& input, It it, It it_end) -> It
            {
                deserialize_binding< B > bind;
                bind.initialize(input);
                it = serializer< deserialize_binding_type, Fmt >::deserialize_iter(bind.use(), it, it_end);
                bind.finalize();
                return it;
            }
        };

        template < vectorlike V, typename Ft, typename Fsz >
        class serializer< V, format_count_list< Ft, Fsz > >
        {
            //     static_assert(! std::is_same_v<Ft, char8_t> );
          public:
            template < typename It >
            static auto serialize_iter(V const& input, It it) -> It
            {
                std::size_t size = input.size();
                serializer< std::size_t, Fsz > size_serializer;
                it = size_serializer.serialize_iter(size, it);
                for (auto& element : input)
                {
                    serializer< std::remove_cvref_t< typeof(element) >, Ft > element_serializer;
                    it = element_serializer.serialize_iter(element, it);
                }
                return it;
            }

            template < typename It >
            static auto deserialize_iter(V& output, It it) -> It
            {
                output.clear();
                std::size_t size = 0;
                it = serializer< std::size_t, Fsz >::deserialize_iter(size, it);
                output.reserve(size);
                for (std::size_t i = 0; i < size; i++)
                {
                    typename V::value_type element;
                    it = serializer< typeof(element), Ft >::deserialize_iter(element, it);
                    output.push_back(std::move(element));
                }
                return it;
            }

            template < typename It >
            static auto deserialize_iter(V& output, It it, It it_end) -> It
            {
                output.clear();
                std::size_t size = 0;
                it = serializer< std::size_t, Fsz >::deserialize_iter(size, it, it_end);
                output.reserve(size);
                for (std::size_t i = 0; i < size; i++)
                {
                    typename V::value_type element;
                    it = serializer< typeof(element), Ft >::deserialize_iter(element, it, it_end);
                    output.push_back(std::move(element));
                }
                return it;
            }
        };

        template < setlike S, typename Ft, typename Fsz >
        class serializer< S, format_count_list< Ft, Fsz > >
        {
            static_assert(setlike< S >);
            static_assert(!std::is_same_v< Ft, char8_t >);

          public:
            template < typename It >
            static auto serialize_iter(S const& input, It it) -> It
            {
                std::size_t size = input.size();
                serializer< std::size_t, Fsz > size_serializer;
                it = size_serializer.serialize_iter(size, it);
                for (auto& element : input)
                {
                    serializer< std::remove_cvref_t< typeof(element) >, Ft > element_serializer;
                    it = element_serializer.serialize_iter(element, it);
                }
                return it;
            }

            template < typename It >
            static auto serialize_iter(S const& input, It it, It it_end) -> It
            {
                std::size_t size = input.size();
                serializer< std::size_t, Fsz > size_serializer;
                it = size_serializer.serialize_iter(size, it, it_end);
                for (auto& element : input)
                {
                    serializer< std::remove_cvref_t< typeof(element) >, Ft > element_serializer;
                    it = element_serializer.serialize_iter(element, it, it_end);
                }
                return it;
            }

            template < typename It >
            static auto deserialize_iter(S& output, It it) -> It
            {
                output.clear();
                std::size_t size = 0;
                it = serializer< std::size_t, Fsz >::deserialize_iter(size, it);
                for (std::size_t i = 0; i < size; i++)
                {
                    typename S::value_type element;
                    it = serializer< typeof(element), Ft >::deserialize_iter(element, it);
                    output.insert(std::move(element));
                }
                return it;
            }

            template < typename It >
            static auto deserialize_iter(S& output, It it, It it_end) -> It
            {
                output.clear();
                std::size_t size = 0;
                it = serializer< std::size_t, Fsz >::deserialize_iter(size, it, it_end);
                for (std::size_t i = 0; i < size; i++)
                {
                    typename S::value_type element;
                    it = serializer< typeof(element), Ft >::deserialize_iter(element, it, it_end);
                    output.insert(std::move(element));
                }
                return it;
            }
        };

        template < maplike M, typename FK, typename FV, typename Fsz >
        class serializer< M, format_count_list< format_sequence< FK, FV >, Fsz > >
        {
            static_assert(maplike< M >);
            using K = std::remove_cvref_t< typename M::key_type >;
            using V = std::remove_cvref_t< typename M::mapped_type >;
            using Ft = format_sequence< FK, FV >;

          public:
            template < typename It >
            static auto serialize_iter(M const& input, It it) -> It
            {
                std::size_t size = input.size();
                serializer< std::size_t, Fsz > size_serializer;
                it = size_serializer.serialize_iter(size, it);
                for (auto& element : input)
                {
                    serializer< std::remove_cvref_t< typeof(element) >, Ft > element_serializer;
                    it = element_serializer.serialize_iter(element, it);
                }
                return it;
            }

            template < typename It >
            static auto serialize_iter(M const& input, It it, It it_end) -> It
            {
                std::size_t size = input.size();
                serializer< std::size_t, Fsz > size_serializer;
                it = size_serializer.serialize_iter(size, it, it_end);
                for (auto& element : input)
                {
                    serializer< std::remove_cvref_t< typeof(element) >, rpnx::serial3::format_sequence< FK, FV > > element_serializer;
                    it = element_serializer.serialize_iter(element, it, it_end);
                }
                return it;
            }

            template < typename It >
            static auto deserialize_iter(M& output, It it) -> It
            {
                output.clear();
                std::size_t size = 0;
                it = serializer< std::size_t, Fsz >::deserialize_iter(size, it);
                for (std::size_t i = 0; i < size; i++)
                {
                    K key;
                    V val;
                    it = serializer< K, FK >::deserialize_iter(key, it);
                    it = serializer< V, FV >::deserialize_iter(val, it);
                    output.emplace(std::piecewise_construct, std::forward_as_tuple(std::move(key)), std::forward_as_tuple(std::move(val)));
                }
                return it;
            }

            template < typename It >
            static auto deserialize_iter(M& output, It it, It it_end) -> It
            {
                output.clear();
                std::size_t size = 0;
                it = serializer< std::size_t, Fsz >::deserialize_iter(size, it, it_end);
                for (std::size_t i = 0; i < size; i++)
                {
                    K key;
                    V val;
                    it = serializer< K, FK >::deserialize_iter(key, it, it_end);
                    it = serializer< V, FV >::deserialize_iter(val, it, it_end);
                    output.emplace(std::piecewise_construct, std::forward_as_tuple(std::move(key)), std::forward_as_tuple(std::move(val)));
                }
                return it;
            }
        };

        template < std::integral I, std::size_t N >
        class serializer< I, format_little_endian< N > >
        {
          public:
            // static_assert(N == sizeof(I) * 8, "This restriction should be removed later");
            template < typename It >
            static constexpr auto serialize_iter(I input, It it)
            {
                for (int i = 0; i < N / 8; i++)
                {
                    *it++ = io_proxy((input >> (i * 8)) & 0xFF);
                }
                return it;
            }

            template < typename It >
            static constexpr auto serialize_iter(I input, It it, It it_end)
            {
                for (int i = 0; i < N / 8; i++)
                {
                    if (it == it_end)
                        throw std::out_of_range("serialize error");
                    *it++ = (input >> (i * 8)) & 0xFF;
                }
                return it;
            }

            template < typename It >
            static constexpr auto deserialize_iter(I& output, It input)
            {
                I output_tmp = 0;
                for (int i = 0; i < N / 8; i++)
                {
                    output_tmp += (*input++) << (i * 8);
                }
                output = output_tmp;
                return input;
            }

            template < typename It >
            static constexpr auto deserialize_iter(I& output, It input, It input_end)
            {
                I output_tmp = 0;
                for (int i = 0; i < N / 8; i++)
                {
                    if (input == input_end)
                        throw std::out_of_range("input range error");
                    output_tmp += (*input++) << (i * 8);
                }
                output = output_tmp;
                return input;
            }
        };

        template <>
        class serializer< std::byte, std::byte >
        {
          public:
            template < typename It >
            static constexpr auto serialize_iter(std::byte input, It it)
            {
                *it++ = io_proxy(input);
                return it;
            }

            template < typename It >
            static constexpr auto serialize_iter(std::byte input, It it, It it_end)
            {
                if (it == it_end)
                    throw std::out_of_range("serialize_iter");
                *it++ = io_proxy(input);
                return it;
            }

            template < typename It >
            static constexpr auto deserialize_iter(std::byte& output, It it)
            {
                output = io_proxy(*it++);
                return it;
            }

            template < typename It >
            static constexpr auto deserialize_iter(std::byte& output, It it, It it_end)
            {
                if (it == it_end)
                    throw std::out_of_range("deserialize_iter");
                output = io_proxy(*it++);
                return it;
            }
        };

        static_assert(std::integral< char >);

        template < std::integral I, std::size_t N >
        class serializer< I, format_big_endian< N > >
        {
            // static_assert(sizeof(I) == N / 8, "This restriction should be removed later");

          public:
            template < typename It >
            static constexpr auto serialize_iter(I input, It it) -> It
            {
                static_assert(N % 8 == 0, "When using serializer, concept Iterator does not support"
                                          " bit-packing, use a BitStream instead of Iterator");
                // N1 is how many bytes are in the format
                constexpr std::size_t N1 = N / 8;
                // N2 is how many bytes are in the object
                constexpr std::size_t N2 = sizeof(I);

                // If object smaller than format, pad with zeros
                if constexpr (N2 < N1)
                {
                    for (int i = 0; i < N1 - N2; i++)
                    {
                        *it++ = io_proxy(0);
                    }
                }
                // If the object is larger than the format, it's possible it could overflow
                // Check if that's the case and then throw an exception if it is
                if constexpr (N2 > N1)
                {
                    if (input >> (N1 * 8) != 0)
                    {
                        throw std::out_of_range("serialize_iter input value too large to be represented in the target format");
                    }
                }

                for (int i = 0; i < N / 8; i++)
                {
                    *it++ = io_proxy((I(input) >> ((N / 8 - i - 1) * 8)) & 0xFF);
                }

                return it;
            }

            // TODO: Other bounds checking.
            template < typename It >
            static constexpr auto deserialize_iter(I& output, It input)
            {
                I output_tmp = 0;
                for (int i = 0; i < N / 8; i++)
                {
                    output_tmp += I(*input++) << ((N / 8 - i - 1) * 8);
                }
                output = output_tmp;
                return input;
            }

            template < typename It >
            static constexpr auto deserialize_iter(I& output, It input, It input_end)
            {
                I output_tmp = 0;
                for (int i = 0; i < N / 8; i++)
                {
                    if (input == input_end)
                        throw std::out_of_range("input range error");
                    output_tmp += I(*input++) << ((N / 8 - i - 1) * 8);
                }
                output = output_tmp;
                return input;
            }
        };

        template < std::integral I >
        class serializer< I, format_uintany >
        {
          public:
            template < typename It >
            static constexpr auto serialize_iter(I input, It out) -> It
            {
                uintmax_t base = input;
                uintmax_t bytecount = 1;
                uintmax_t max = 0;
                max = (1ull << (7)) - 1;
                while (base > max)
                {
                    bytecount++;
                    base -= max + 1;
                    max = (1ull << ((7) * bytecount)) - 1;
                }
                for (uintmax_t i = 0; i < bytecount; i++)
                {
                    uint8_t val = base & ((uintmax_t(1) << (7)) - 1);
                    if (i != bytecount - 1)
                    {
                        val |= 0b10000000;
                    }
                    *out++ = io_proxy(val);
                    base >>= 7;
                }
                return out;
            }

            template < typename It >
            static constexpr auto deserialize_iter(I& output, It input) -> It
            {
                output = 0;
                uintmax_t n2 = 0;
                while (true)
                {
                    std::uint8_t a = *input++;
                    std::uint8_t read_value = a & 0b1111111;
                    output += (uintmax_t(read_value) << (n2 * 7));
                    if (!(a & 0b10000000))
                        break;
                    n2++;
                }
                for (uintmax_t i = 1; i <= n2; i++)
                {
                    output += (uintmax_t(1) << (i * 7));
                }
                return input;
            }

            template < typename It >
            static constexpr auto deserialize_iter(I& output, It input, It input_end) -> It
            {
                output = 0;
                uintmax_t n2 = 0;
                while (true)
                {
                    if (input == input_end)
                        throw std::out_of_range("deserialization input range error");
                    std::uint8_t a = *input++;
                    std::uint8_t read_value = a & 0b1111111;
                    output += (uintmax_t(read_value) << (n2 * 7));
                    if (!(a & 0b10000000))
                        break;
                    n2++;
                }
                for (uintmax_t i = 1; i <= n2; i++)
                {
                    output += (uintmax_t(1) << (i * 7));
                }
                return input;
            }
        };

        template < typename A1, typename A2, typename B1, typename B2 >
        class serializer< std::pair< A1, A2 >, format_sequence< B1, B2 > >
        {
          public:
            template < typename It >
            static auto serialize_iter(std::pair< A1, A2 > const& input, It it) -> It
            {
                using A1_t = std::remove_cvref_t< A1 >;
                using A2_t = std::remove_cvref_t< A2 >;
                it = serializer< A1_t, B1 >::serialize_iter(input.first, it);
                it = serializer< A2_t, B2 >::serialize_iter(input.second, it);
                return it;
            }

            template < typename It >
            static auto serialize_iter(std::pair< A1, A2 > const& input, It it, It it_end) -> It
            {
                using A1_t = std::remove_cvref_t< A1 >;
                using A2_t = std::remove_cvref_t< A2 >;
                it = serializer< A1_t, B1 >::serialize_iter(input.first, it, it_end);
                it = serializer< A2_t, B2 >::serialize_iter(input.second, it, it_end);
                return it;
            }

            template < typename It >
            static auto deserialize_iter(std::pair< A1, A2 >& output, It it, It it_end) -> It
            {
                using A1_t = std::remove_cvref_t< A1 >;
                using A2_t = std::remove_cvref_t< A2 >;
                it = serializer< A1_t, B1 >::deserialize_iter(output.first, it, it_end);
                it = serializer< A2_t, B2 >::deserialize_iter(output.second, it, it_end);
                return it;
            }

            template < typename It >
            static auto deserialize_iter(std::pair< A1, A2 >& output, It it) -> It
            {
                using A1_t = std::remove_cvref_t< A1 >;
                using A2_t = std::remove_cvref_t< A2 >;
                it = serializer< A1_t, B1 >::deserialize_iter(output.first, it);
                it = serializer< A2_t, B2 >::deserialize_iter(output.second, it);
                return it;
            }
        };

        template < typename Fsz >
        class serializer< std::u8string, format_utf8< Fsz > >
        {
            using Ft = format_utf8< Fsz >;
            using Et = char8_t;

          public:
            template < typename It >
            static auto serialize_iter(std::u8string const& input, It it) -> It
            {
                std::size_t size = input.size();
                serializer< std::size_t, Fsz > size_serializer;
                it = size_serializer.serialize_iter(size, it);
                for (auto& element : input)
                {
                    serializer< uint8_t > element_serializer;
                    it = element_serializer.serialize_iter(element, it);
                }
                return it;
            }

            template < typename It >
            static auto deserialize_iter(std::u8string& output, It it) -> It
            {
                output.clear();
                std::size_t size = 0;
                it = serializer< std::size_t, Fsz >::deserialize_iter(size, it);
                output.reserve(size);
                for (std::size_t i = 0; i < size; i++)
                {
                    Et element;
                    it = serializer< typeof(element), Ft >::deserialize_iter(element, it);
                    output.push_back(std::move(element));
                }
                return it;
            }

            template < typename It >
            static auto deserialize_iter(std::u8string& output, It it, It it_end) -> It
            {
                output.clear();
                std::size_t size = 0;
                it = serializer< std::size_t, Fsz >::deserialize_iter(size, it, it_end);
                output.reserve(size);
                for (std::size_t i = 0; i < size; i++)
                {
                    uint8_t element;
                    it = serializer< uint8_t >::deserialize_iter(element, it, it_end);
                    output.push_back(char8_t(element));
                }
                return it;
            }
        };

        template < typename... Ts, typename... Ts2 >
        class serializer< std::tuple< Ts... >, format_sequence< Ts2... > >
        {
            template < size_t N, typename It >
            static auto subserialize_iter(std::tuple< Ts... > const& input, It it) -> It
            {
                if constexpr (N < sizeof...(Ts))
                {
                    auto& n = std::get< N >(input);
                    it = serializer< std::remove_cvref_t< typeof(n) >, std::remove_cvref_t< typename std::tuple_element< N, std::tuple< Ts2... > >::type > >::serialize_iter(n, it);
                    return subserialize_iter< N + 1 >(input, it);
                }
                return it;
            }

            template < size_t N, typename It >
            static auto subdeserialize_iter(auto&& output, It it) -> It
            {
                if constexpr (N < sizeof...(Ts))
                {
                    auto& n = std::get< N >(output);
                    it = serializer< std::remove_reference_t< typeof(n) >, std::remove_cvref_t< typename std::tuple_element< N, std::tuple< Ts2... > >::type > >::deserialize_iter(n, it);
                    return subdeserialize_iter< N + 1 >(output, it);
                }
                return it;
            }

            template < size_t N, typename It >
            static auto subdeserialize_iter(auto&& output, It it, It it_end) -> It
            {
                if constexpr (N < sizeof...(Ts))
                {
                    auto& n = std::get< N >(output);
                    it = serializer< std::remove_cvref_t< typeof(n) >, std::remove_cvref_t< typename std::tuple_element< N, std::tuple< Ts2... > >::type > >::deserialize_iter(n, it, it_end);
                    return subdeserialize_iter< N + 1 >(output, it, it_end);
                }
                return it;
            }

          public:
            template < typename It >
            static constexpr auto serialize_iter(auto&& tuple, It it) -> It
            {
                return subserialize_iter< 0 >(tuple, it);
            }

            template < typename It >
            static constexpr auto serialize_iter(auto&& tuple, It it, It it_end) -> It
            {
                return subserialize_iter< 0 >(tuple, it, it_end);
            }

            template < typename It >
            static constexpr auto deserialize_iter(auto&& tuple, It it) -> It
            {
                return subdeserialize_iter< 0 >(tuple, it);
            }

            template < typename It >
            static constexpr auto deserialize_iter(auto&& tuple, It it, It it_end) -> It
            {
                return subdeserialize_iter< 0 >(tuple, it, it_end);
            }
        };

        template < arraylike A, typename F, std::size_t N >
        class serializer< A, format_array< F, N > >
        {

            using value_type = typename A::value_type;
            using normal_value_type = std::remove_cvref_t< value_type >;

            static constexpr std::size_t size()
            {
                return std::tuple_size_v< A >;
            }

          public:
            template < typename It >
            static constexpr auto serialize_iter(auto&& tuple, It it) -> It
            {
                for (std::size_t i = 0; i < size(); i++)
                {
                    it = serializer< normal_value_type, F >::serialize_iter(tuple[i], it);
                }
                return it;
            }

            template < typename It >
            static constexpr auto serialize_iter(auto&& tuple, It it, It it_end) -> It
            {
                for (std::size_t i = 0; i < size(); i++)
                {
                    it = serializer< normal_value_type, F >::serialize_iter(tuple[i], it, it_end);
                }
                return it;
            }

            template < typename It >
            static constexpr auto deserialize_iter(auto&& tuple, It it) -> It
            {
                for (std::size_t i = 0; i < size(); i++)
                {
                    it = serializer< normal_value_type, F >::deserialize_iter(tuple[i], it);
                }
                return it;
            }

            template < typename It >
            static constexpr auto deserialize_iter(auto&& tuple, It it, It it_end) -> It
            {
                for (std::size_t i = 0; i < size(); i++)
                {
                    it = serializer< normal_value_type, F >::deserialize_iter(tuple[i], it, it_end);
                }
                return it;
            }
        };

        template < typename T, typename Format = typename default_format< T >::type, typename It >
        auto iterator_serialize(T const& in, It it) -> It
        {
            return serializer< T, Format >::serialize_iter(in, it);
        }

        template < typename T, typename Format = typename default_format< T >::type, typename It >
        auto iterator_deserialize(T& out, It it) -> It
        {
            return serializer< T, Format >::deserialize_iter(out, it);
        }

        template < typename T, typename Format = typename default_format< T >::type, typename It >
        auto iterator_deserialize(T& out, It it, It it_end) -> It
        {
            return serializer< T, Format >::deserialize_iter(out, it, it_end);
        }

        template < typename Format, typename T, typename It >
        auto iterator_deserialize_fmt(T&& out, It it, It it_end) -> It
        {
            return serializer< std::remove_reference_t< T >, Format >::deserialize_iter(std::forward< T&& >(out), it, it_end);
        }

        template < typename Format, typename T, typename It >
        auto iterator_deserialize_fmt(T&& out, It it) -> It
        {
            return serializer< std::remove_reference_t< T >, Format >::deserialize_iter(std::forward< T&& >(out), it);
        }

        template < typename Format, typename T, typename It >
        auto iterator_serialize_fmt(T const& in, It it) -> It
        {
            static_assert(!std::is_same_v< Format, char8_t >);
            return serializer< std::remove_cvref_t< T >, Format >::serialize_iter(in, it);
        }
    } // namespace serial3
} // namespace rpnx

static_assert(rpnx::serial3::vectorlike< std::u8string >);

#endif // RPNX_SERIAL_SERIAL_INTERFACE_HPP
