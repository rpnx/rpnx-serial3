//
// Created by rnicholl on 1/17/22.
//

#include "gtest/gtest.h"
#include "rpnx/serial3.hpp"

static_assert(! rpnx::serial3::setlike<std::map<unsigned int, std::u8string > >);
using bar = typename rpnx::serial3::default_format<std::map<unsigned int, std::u8string > >::type ;
struct my_foo
{
    std::u8string foo;
    std::u8string bar;
    int64_t baz;
    int64_t buz;
    auto operator ==(my_foo const & other) const noexcept
    {
        return std::tie(foo, bar, baz, buz) == std::tie(other.foo, other.bar, other.baz, other.buz);
    }

    bool operator !=(my_foo const &) const = default;
};


template <>
struct rpnx::serial3::serialize_binding<my_foo>
{
    my_foo const * m_ptr =nullptr;
public:
    void initialize(my_foo const & foo) { m_ptr = & foo; }
    auto use () { return std::tie(m_ptr->foo, m_ptr->bar, m_ptr->baz, m_ptr->buz); }
    void finalize() {}
};

template <>
struct rpnx::serial3::deserialize_binding<my_foo>
{
    my_foo * m_ptr =nullptr;
public:
    void initialize(my_foo & foo) { m_ptr = &foo; }
    auto use () { return std::tie(m_ptr->foo, m_ptr->bar, m_ptr->baz, m_ptr->buz); }
    void finalize() {}
};

TEST(rpnx_serial3, endians)
{
    EXPECT_TRUE(1 == 1);
    std::uint64_t value = 1;

    std::vector<std::uint8_t> output;
    auto out_inserter = std::back_inserter (output);
    rpnx::serial3::serializer<std::uint64_t, rpnx::serial3::format_little_endian<64> >
            serializer;
    serializer.serialize_iter(value, out_inserter);

    EXPECT_EQ(output.size(), 8);
    EXPECT_EQ(output[0], 1);
    EXPECT_EQ(output[1], 0);
    EXPECT_EQ(output[2], 0);
    EXPECT_EQ(output[3], 0);
    EXPECT_EQ(output[4], 0);
    EXPECT_EQ(output[5], 0);
    EXPECT_EQ(output[6], 0);
    EXPECT_EQ(output[7], 0);

    output.clear();

    value = 9;

    rpnx::serial3::serializer<std::uint64_t,
    rpnx::serial3::format_little_endian<32> >
            ::serialize_iter(value, out_inserter);

    EXPECT_EQ(output.size(), 4);
    EXPECT_EQ(output[0], 9);
    EXPECT_EQ(output[1], 0);
    EXPECT_EQ(output[2], 0);
    EXPECT_EQ(output[3], 0);

    output.clear();

    rpnx::serial3::serializer<std::uint64_t,
            rpnx::serial3::format_big_endian<32> >
    ::serialize_iter(value, out_inserter);

    EXPECT_EQ(output.size(), 4);
    EXPECT_EQ(output[0], 0);
    EXPECT_EQ(output[1], 0);
    EXPECT_EQ(output[2], 0);
    EXPECT_EQ(output[3], 9);
}
TEST(rpnx_serial3, string)
{
    std::u8string hello = u8"hello";

    std::vector<std::uint8_t> output;
    auto out_inserter = std::back_inserter (output);
    rpnx::serial3::iterator_serialize(hello, out_inserter);

}
TEST(rpnx_serial3, vectors)
{

    std::vector<std::uint16_t> values {4, 5, 6};

    std::vector<std::uint8_t> output;
    auto out_inserter = std::back_inserter (output);

    static_assert(rpnx::serial3::vectorlike<typeof(values)>);
    static_assert(rpnx::serial3::vectorlike<std::string>);
    rpnx::serial3::serializer<
            typeof(values)        >::serialize_iter(values, out_inserter);



    EXPECT_EQ(output.size(), 7);
    EXPECT_EQ(output[0], 3);
    EXPECT_EQ(output[1], 4);
    EXPECT_EQ(output[2], 0);
    EXPECT_EQ(output[3], 5);
    EXPECT_EQ(output[4], 0);
    EXPECT_EQ(output[5], 6);
    EXPECT_EQ(output[6], 0);



    std::vector<std::uint64_t> values2 {4, 5, 6};

    output.clear();
    rpnx::serial3::serializer<
            typeof(values2),
            rpnx::serial3::format_count_list<rpnx::serial3::format_little_endian<16>>
            >::serialize_iter(values2, out_inserter);
    EXPECT_EQ(output.size(), 7);
    EXPECT_EQ(output[0], 3);
    EXPECT_EQ(output[1], 4);
    EXPECT_EQ(output[2], 0);
    EXPECT_EQ(output[3], 5);
    EXPECT_EQ(output[4], 0);
    EXPECT_EQ(output[5], 6);
    EXPECT_EQ(output[6], 0);

    output.clear();
    rpnx::serial3::serializer<
            typeof(values2),
            rpnx::serial3::format_count_list<rpnx::serial3::format_big_endian<16>>
    >::serialize_iter(values2, out_inserter);
    EXPECT_EQ(output.size(), 7);
    EXPECT_EQ(output[0], 3);
    EXPECT_EQ(output[1], 0);
    EXPECT_EQ(output[2], 4);
    EXPECT_EQ(output[3], 0);
    EXPECT_EQ(output[4], 5);
    EXPECT_EQ(output[5], 0);
    EXPECT_EQ(output[6], 6);

    std::vector<std::uint64_t> values3;

    rpnx::serial3::iterator_deserialize_fmt
            <rpnx::serial3::format_count_list<rpnx::serial3::format_big_endian<16>>>(values3, output.begin(), output.end());

    EXPECT_EQ(values2, values3);
}

TEST(rpnx_serial3, tuples) {
    std::vector<std::uint8_t> output;
    auto out_inserter = std::back_inserter(output);

    std::uint64_t a = 4;
    std::uint64_t b = 2;

    output.clear();
    rpnx::serial3::iterator_serialize(std::tie(a, b), out_inserter);
    EXPECT_EQ(output.size(), 16);

    output.clear();

    rpnx::serial3::iterator_serialize_fmt<
            rpnx::serial3::format_sequence<
                    rpnx::serial3::format_little_endian<16>,
                    rpnx::serial3::format_big_endian<16>
            >>(std::tie(a, b), out_inserter);

    EXPECT_EQ(output.size(), 4);
    EXPECT_EQ(output[0], 4);
    EXPECT_EQ(output[1], 0);
    EXPECT_EQ(output[2], 0);
    EXPECT_EQ(output[3], 2);

    std::uint64_t c;
    std::uint64_t d;

    rpnx::serial3::iterator_deserialize_fmt<rpnx::serial3::format_sequence<
            rpnx::serial3::format_little_endian<16>,
            rpnx::serial3::format_big_endian<16>
    >>(std::tie(c, d), output.begin(), output.end() );
}

TEST(rpnx_serial3, maps) {
    std::vector<std::uint8_t> output;
    auto out_inserter = std::back_inserter(output);

    std::uint64_t a = 4;
    std::uint64_t b = 2;

    std::map<std::uint32_t, std::u8string> mapy = { {4 , u8"hello"} , { 7, u8"penguin" } };

    output.clear();
    rpnx::serial3::iterator_serialize(mapy, out_inserter);
    EXPECT_EQ(output.size(), 23);

    output.clear();

}


TEST(rpnx_serial3, custom) {
    std::vector<std::uint8_t> output;
    auto out_inserter = std::back_inserter(output);

    my_foo f;
    f.foo = u8"cat";
    f.bar = u8"dog";
    f.baz = 2;
    f.buz = 9;
    using foo_format = rpnx::serial3::format_sequence<
            typename rpnx::serial3::default_format<std::u8string>::type,
            typename rpnx::serial3::default_format<std::u8string>::type,
            rpnx::serial3::format_little_endian<32>,
            rpnx::serial3::format_little_endian<16>
                    >;
    static_assert(rpnx::serial3::bindable<my_foo>);
    rpnx::serial3::iterator_serialize_fmt<foo_format>(f, out_inserter);
    EXPECT_EQ(output.size(), 14);

    my_foo f2;

    rpnx::serial3::iterator_deserialize_fmt<foo_format>(f2, output.begin(), output.end());

    EXPECT_EQ(f, f2);

    output[1] = 'f';

    rpnx::serial3::iterator_deserialize_fmt<foo_format>(f2, output.begin(), output.end());

    EXPECT_NE(f, f2);

    EXPECT_EQ(f2.foo, std::u8string (u8"fat"));

}